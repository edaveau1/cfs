On rédige une première mouture du funk.c :
```
int
add(int a, int b)
{
        return a+b;
}

int
main()
{
        return add(0, 1);
}
```

Puis on compile, on lance, et on obtient bien le code de retour 1 (ou peu importe, en fonction de ce qui se trouve dans la fonction add()).

On réécrit le programme, mais cette fois-ci en déclarant add() après notre main :
```
int
main()
{
        return add(0, 1);
}

int
add(int a, int b)
{
        return a+b;
}
```

A ce moment là, la compilation fonctionne, mais nous indique __warning: déclaration implicite de la fonction « add »__.
La norme veut qu'en C on aie avant la fonction main() un prototype de la fonction que l'on veut utiliser.

Pour permettre au compilateur de ne pas renvoyer de warning, il faut alors déclarer le prototype de la fonction. Notre fichier ressemblerait alors.
```
int add(int, int);

int
main()
{
        return add(2, 1);
}

int
add(int a, int b)
{
        return a+b;
}

```

Et le programme fonctionnerait correctement sans renvoyer de warning.

On reprend donc le programme dans l'ordre initial, mais cette fois-ci on va venir créer un fichier, add.c, dans lequel on va déplacer la fonction add.
On laisse le prototype dans le funk.c, et on compile de la manière suivante : `cc -m32 -o funk funk.c add.c`

Imaginons maintenant que l'on ait plusieurs fonctions génériques que l'on souhaite utiliser dans différents programmes.
Des bibliothèques partagées permettant à plusieurs programmes d'utiliser les mêmes fonctions sans avoir à les recompiler ou à les c/c plusieurs fois.

Pour faire cela, on utilise le compilateur et on créé un objet qui va être une librairie :
`cc -shared -o libadd.so add.c`
Cela nous créé la lib add.so.
Maintenant, au lieu de compiler funk.c et add.c et de les lier ensemble, on va compiler funk uniquement en lui disant qu'il dépend d'une
librairie partagée appelée la libadd. On pourrait ainsi compiler de la manière suivante :
```
$ cc -o funk funk.c -ladd
/usr/bin/ld: cannot find -ladd
collect2: error: ld returned 1 exit status
```

La libadd n'est pas installée dans un endroit connu (/usr/local/lib, /usr/lib...). On lui préciser donc le répertoire où trouver la lib en question :
`cc -o funk funk.c -L. -ladd`

Si on regarde les dépendances de funk :
```
$ ldd funk
	linux-vdso.so.1 (0x00007ffe4c7bd000)
	libadd.so => not found
	libc.so.6 => /lib64/libc.so.6 (0x00007fc98326b000)
	/lib64/ld-linux-x86-64.so.2 (0x00007fc983630000)
```

Donc si on essaie de lancer funk, on obtient :
`./funk: error while loading shared libraries: libadd.so: cannot open shared object file: No such file or directory`

Par contre, on peut utiliser une variable d'environnement liée aux variables dynamiques, LD_LIBRARY_PATH, à laquelle on peut assigner des répertoires contenant une librairie.
```
$ LD_LIBRARY_PATH=. ./funk
$ echo $?
3
```

Après, on n'a pas forcément envie de mettre le prototype à chaque fois dans le .c.
A la place, on va donc créer un header, add.h, dans lequel on va définir un prototype. 
Et ce qu'on fait dans les codes où l'on veut utiliser les fonctions en question, c'est qu'on appelle une directive qui va être complétée par le préprocesseur.

funk.c
```
#include add.h

int
main()
{
	return add(2, 1);
}
```

add.h
```
int add(int, int);
```

L'include permet d'inclure la définition des fonctions dans tous les codes où l'on souhaite utiliser les fonctions de notre librairie.
Lorsqu'on compile un programme, il y a plusieurs étapes, la première d'entre elle étant celle du préprocesseur.
Le préprocesseur est une sorte de gros sed. Il permet de remplacer des trucs.
On peut voir ce qu'il fait avec `cc -E funk.c` :
```
# 1 "funk.c"
# 1 "<interne>"
# 1 "<ligne-de-commande>"
# 31 "<ligne-de-commande>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<ligne-de-commande>" 2
# 1 "funk.c"
# 1 "add.h" 1
int add(int, int);
# 2 "funk.c" 2

int
main()
{
        return add(2, 1);
}
```

On peut voir ici :
```
# 1 "add.h" 1
int add(int, int);
```

add.h est ici remplacé par le prototype de add.
Le préprocesseur est appelé partout où il y a un '#'.

On rebuilde :
`cc -o funk funk.c -ladd` (on peut utiliser le flag -L. ou pas, le résultat sera le même)

On peut faire d'autres actions dans un header qu'include. Plusieurs directives (macros) existent, comme `#define` :

Dans `add.h`, on rajoute :
```
#define PLOP 1
```

Cela viendra remplacer tous les "PLOP" par 1. Dans funk.c, on modifie le return ainsi :
```
	return add(2, PLOP);
```

On recompile et on relance avec LD_LIBRARY_PATH.

Par défaut, un programme compilé va systématiquement, lorsqu'il s'agit d'un programme lié dynamiquement, lier le programme à la libc.
Ainsi, techniquement, un programme lié à la libc a accès à l'intégralité des fonctions (eg. printf, ...).
Une des fonctions de la libc est write. Cette fonction est une des plus bas niveau pour écrire quelque chose sur un descripteur de fichier.
Cette fonction prend 3 paramètres et renvoie un truc de type size_t :
	* Un file descriptor
	* Le truc qu'on veut écrire
	* La taille du truc qu'on veut écrire

Les file descriptors les plus connus sont :
	* 1 : stdout (_écran_)
	* 2 : stderr

funk.c
```
#include add.h

int
main()
{
	write(1, "ASM PHENOMENAAAA\n", 18);
	return add(2, PLOP);
}
```

On compile : `cc -o funk funk.c -L. -ladd` :
```
funk.c: Dans la fonction « main »:
funk.c:6:9: warning: déclaration implicite de la fonction « write » [-Wimplicit-function-declaration]
         write(1, "ASM PHENOMENAAAA\n", 18);
         ^~~~~
```

Techniquement ça a compilé, mais on voit que l'import de write n'est pas inclus directement.
On va donc définir le prototype de la fonction write dans notre add.h :

add.h
```
#define PLOP 1

int add(int, int);
int write(int, char *, int);
```

Et on recompile. Pas de soucis. Si on reprend notre lancement avec LD_LIBRARY_PATH :
```
LD_LIBRARY_PATH=. ./funk
ASM PHENOMENAAAA
```

On a écrit quelque chose sur stdout. Si on reprend les liens de notre exécutable :
```
$ ldd funk
	linux-vdso.so.1 (0x00007ffd371f6000)
	libadd.so => not found
	libc.so.6 => /lib64/libc.so.6 (0x00007fd94cf83000)
	/lib64/ld-linux-x86-64.so.2 (0x00007fd94d348000)
```

On a donc la libc qui exporte tout un tas de fonctions. Tant qu'on n'a pas déclaré les prototypes de ces fonctions, on y aura accès, mais en warning. De manière dégueulasse.

