/*
La fonction main n'est pas le vrai point d'entrée du programme.
Lorsqu'on écrit un programme, on écrit une fonction, qui s'appelle main.
Ce main est appelé en général par un programme qui s'appelle ld.

Originellement, ld est l'abbréviation de loader et link editor
  - Le compilateur génère du code machine depuis le code source
  - Le linker combine ces fichiers de code machine pour en faire un exécutable

Pour faire simple, le linker agit lorsqu'un fichier '.obj' doit être lié avec
les fonctions de sa librairie. Le compilateur ne fait que convertir un fichier
.c en fichier .obj sans comprendre les fonctions utilisées.
Ainsi, pour rendre le fichier '.obj' exécutable (donc en '.exe'),
on a besoin du linker qui va aider le compilateur à comprendre des fonctions
de librairies.
*/

int
main()
{
	return 42;
}

/*
Ici, on déclare une fonction main qui retourne un type de donnée renvoyé par la fonction (int).
Ce point est particulièrement important car il fait de C un langage typé.
Si l'on ne précisait pas int, implicitement main renverrait de l'int.

Ce type est renvoyé au processus PARENT du programme.

Le return permet de renvoyer une valeur pour dire si le programme s'est bien exécuté ou non
*/

/*
Par rapport à la valeur de retour:
  Si l'on mettait 1 à la place de 0, que l'on compilait le programme, et qu'on l'exécutait,
le programme renverrait 1, mais comme il s'est bien exécuté, il renverrait 0.

Les codes de retour sont cruciaux. On pourrait par exemple souhaiter que -1 renvoie à une erreur mémoire,
-2 au fait que l'on soit un jour impair, etc...

A noter : La valeur sortant de la fonction main n'est pas en lien avec le code de retour du programme.
*/

/*
Enfin, on pourrait mettre exit à la place de return.
La différence est qu'exit ne fait pas le ménage derrière elle.
Par exemple, si une fonction déclare la valeur d'une variable,
exit ne reset pas la valeur de cette variable, qui reste disponible en mémoire.
*/
