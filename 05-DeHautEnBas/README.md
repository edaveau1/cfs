# Récapitulatif de ce que l'on a vu jusqu'ici
Reprenons la structure sommaire d'un programme qui se lance. Un programme se lance dans une zone mémoire, un espace d'adressage, avec plusieurs segments (sections) :
[   Kernel space   ]
			<- 0xC0000000 (Adresse de fin du segment)
			<- Random stack offset
[       Stack      ]	<- RLIMIT_STACK (eg. 8MB)
			<- Random mmap offset
[  Memory mapping  ]
			<- Program break
			<- break
[       Heap       ]
			<- start_break
			<- Random break offset
[    BSS segment   ]
			<- end_data
[   Data segment   ]
			<- start_data
			<- end_code
[Text segment (ELF)]
			<- 0x80480000 (Adresse de début du segment. En dessous, on a les adresses réservées pour le "kernel text and data")

## Text :
Ce segment contient le code interprétable par le CPU (section text). C'est du pur binaire, de l'hexadécimal, que le processeur sait interpréter.
Cette zone est en read-only (on ne modifie pas le code pendant qu'il tourne)

## Data :
Données statiques et globales qui sont initialisées.

# Zoomons sur la stack :

La stack est initialement vide (ou presque), quand on arrive dans une fonction. Un des premiers éléments qu'on trouve dans la stack est une adresse,
qui est l'adresse de la fonction appelante (adresse retour).
Le deuxième élément de la pile est le base pointer = Où est-ce qu'on démarre les éléments de la fonction dans laquelle on est (= une frame).
A partir de là, on va empiler des trucs (i, myvar, tc...), sachant que les adresses hautes sont au début de la stack. Quand on empile, on empile vers le bas
(adresse inférieure). On dit donc que la stack croit vers le bas :

[ Adresse retour ] = 0xC0000000
[  Base pointer  ]
[       i        ]
[     myvar      ]
[       tc       ]
[      ...       ]
		  <- 0x804*****

On écrit le simple.c, et on le compile de la manière suivante :
`cc -m32 -O0 -ggdb -o simple -fno-pie simple.c`
Puis on débugge avec gdb avec le plugin gef (gdb enhanced features).
gdb simple
> break main #Ou bien > b main

On peut aussi changer le sens de la stack (par défaut, des plus petites adresses aux plus grandes.
On peut changer ce comportement avec :
> gef config context.grow_stack_down True
> run

L'output va être décomposé :
```
$eax   : 0xf7fb6248  →  0xffffc62c  →  0xffffc847  →  "LS_COLORS=rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;3[...]"
$ebx   : 0xf7fb4000  →  0x001a3d6c
$ecx   : 0xef1d38c3
$edx   : 0xffffc5c4  →  0xf7fb4000  →  0x001a3d6c
$esp   : 0xffffc588  →  0x0804849d  →  <__libc_csu_init+13> add ebx, 0x1b63
$ebp   : 0xffffc598  →  0x00000000
$esi   : 0x0       
$edi   : 0x08048340  →  <_start+0> endbr32 
$eip   : 0x08048483  →  <main+6> mov DWORD PTR [ebp-0x4], 0xa
$eflags: [zero carry PARITY adjust SIGN trap INTERRUPT direction overflow resume virtualx86 identification]
$cs: 0x23 $ss: 0x2b $ds: 0x2b $es: 0x2b $fs: 0x00 $gs: 0x63 
```

Cette partie nous donne les registres du processeur (une case mémoire dans le CPU permettant d'échanger avec la mémoire).
On ne va pas trop s'y intéresser, l'output suivant est plus important :
```
0xffffc588│+0x0000: 0x0804849d  →  <__libc_csu_init+13> add ebx, 0x1b63	 ← $esp
0xffffc58c│+0x0004: 0xf7fb4000  →  0x001a3d6c
0xffffc590│+0x0008: 0x00000000
0xffffc594│+0x000c: 0x08048340  →  <_start+0> endbr32 
0xffffc598│+0x0010: 0x00000000	 ← $ebp
0xffffc59c│+0x0014: 0xf7e2df36  →  <__libc_start_main+246> add esp, 0x10
0xffffc5a0│+0x0018: 0x00000001
0xffffc5a4│+0x001c: 0xffffc624  →  0xffffc813  →  "/home/T0286633-A/cfromscratch/05-DeHautEnBas/simpl[...]"
```

Ceci est la stack. Tout ceci représente notre programme. Ce sont les adresses de la stack jusqu'au point du code où l'on est.
> step
```
0xffffc5a4│+0x001c: 0xffffc624  →  0xffffc813  →  "/home/T0286633-A/cfromscratch/05-DeHautEnBas/simpl[...]"
0xffffc5a0│+0x0018: 0x00000001
0xffffc59c│+0x0014: 0xf7e2df36  →  <__libc_start_main+246> add esp, 0x10
0xffffc598│+0x0010: 0x00000000	 ← $ebp
0xffffc594│+0x000c: 0x0000000a ("\n"?)
0xffffc590│+0x0008: 0x00000000
0xffffc58c│+0x0004: 0xf7fb4000  →  0x001a3d6c
0xffffc588│+0x0000: 0x0804849d  →  <__libc_csu_init+13> add ebx, 0x1b63	 ← $esp
```

On peut voir ici la valeur 10 (0x0000000a) ajoutée à la pile.

> s
```
0xffffc5a4│+0x001c: 0xffffc624  →  0xffffc813  →  "/home/T0286633-A/cfromscratch/05-DeHautEnBas/simpl[...]"
0xffffc5a0│+0x0018: 0x00000001
0xffffc59c│+0x0014: 0xf7e2df36  →  <__libc_start_main+246> add esp, 0x10
0xffffc598│+0x0010: 0x00000000	 ← $ebp
0xffffc594│+0x000c: 0x0000000a ("\n"?)
0xffffc590│+0x0008: 0x00000000
0xffffc58c│+0x0004: 0xf7fb4000  →  0x001a3d6c
0xffffc588│+0x0000: 0x0804849d  →  <__libc_csu_init+13> add ebx, 0x1b63	 ← $esp
```

Si l'on examine le contenu de l'adresse 0xffffc594 :
> x/w 0xffffc594
0xffffc594:	0xa

0xa correspond bien au contenu de la variable que l'on a déclarée.

On ajoute ensuite une variable c à notre code :
...
	char c = 'A';
...
On recompile et on redébugge.
Dans la stack, on obtient alors :
```
0xffffc594│+0x000c: 0x0000000a ("\n"?)		<- 10
0xffffc590│+0x0008: 0x41000000			<- A
```

Si on examine l'adresse de 10 :
> x/w 0xffffc594 (w = word, soit 4 octets)
0xffffc594:	0xa
On pourrait aussi écrire :
> x/4b 0xffffc594
0xffffc594:	0xa	0x0	0x0	0x0
> p &i
$1 = (int *) 0xffffc594

L'adresse dans la stack est bien l'adresse de i. Puisque c'est une adresse, c'est un pointeur, d'où le int * pour dire "A cette adresse là, il y a un int".
