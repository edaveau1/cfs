int
main()
{
	int i = 1024;		/*Dans lldb, avant d'initialiser i, p i affiche une valeur indéfinie (134513472)*/
	char c[4] = "foo";	/*Désormais, p i affiche bien 1024, et l'adresse est la même que pour la valeur indéfinie (eg. 0xfffffcf04)*/

/*
Pour afficher le contenu d'une adresse :
> p *(&i)
(int) $13 = 1024

i est un int composé de 4 octets
> p sizeof(i)
(unsigned int) $10 = 4
Pour inspecter le contenu des 4 octets :
> x/4b i
OU
> x/4b &i (sauf que là on inspecte le contenu des octets à l'adresse &i)
0xffffcf04: 0x00 0x04 0x00 0x00
$ printf "%d\n" 0x0400
1024
Pourquoi 1024 est-il sur le 2ème octet ?
Sur un certain nombre d'architectures, les entiers sont inscrits d'une certaine façon, l'endianness.
L'endianness représente l'ordre de codage des octets (big endian ou little endian). Sur les architectures x86 et 64 bits, on n'écrit pas :
0x0400 pour représenter 1024, mais 0x00 0x40. Le premier octet 0x00 va se retrouver derrière le second octet 0x04, ce qui va donner 0x0400
L'endianness ne concerne que les chiffres (qui dépassent 1 octet).
Ainsi, 0x00 0x04 0x00 0x00 peut se lire : 00000400, soit 1024 en hexadécimal.
Les 4 octets sont donc un entier.

Voici les résultats pour c :
> p sizeof(c)
(unsigned int) $11 = 4
> p sizeof(c[1])
(unsigned int) $12 = 1

Chose intéressante à partir d'ici : L'adresse &i est 0xffffcf04, alors que l'adresse &c est 0xffffcf00.
Ici, &c est situé avant &i en mémoire (à 0xffffcf00, 0xffffcf01, 0xffffcf02 et 0xffffcf03, alors que c est déclaré après ?!
Affichons les 8 octets à partir de 0xffffcf00 :
> x/8b 0xffffcf00
0xffffcf00: 0x66 0x6f 0x6f 0x00 0x00 0x04 0x00 0x00
On peut d'ailleurs lire ci-dessus : foo\01024 :
f(0x66)o(0x6f)o(0x6f)\0(0x00)1024(0x00 0x04 0x00 0x00)
On voit aussi qu'il n'y a pas d'endianness sur les tableaux de caractères. Il n'y a pas besoin de les inverser.

Pour en donner une représentation graphique, on a en mémoire :
[f][o][o][\0][00][04][00][00]
Avec f qui se situe à 0xffffcf00. c pointe sur les 4 premiers emplacements. i sur les 4 derniers.

Ici, c n'est rien d'autre qu'un pointeur vers 0xffffcf00, qui est l'adresse du premier caractère de la chaîne.

La suite dans le 2-2.c
*/

	return i;
}
