int
main()
{
	int i = 1024;
	char c[4] = "foo", *p; /*Ici, on déclare une variable p, qui va contenir une adresse
				NB : On n'est pas obligé de déclarer p ici, on aurait pu le mettre avant ou après cette ligne avec `char *p;`*/

	p = (char *)&c; /*Notre variable p va donc ici contenir l'adresse de notre chaîne p.
			La variable p est une variable char qui va venir pointer => (char *) vers &c
			Donc lorsqu'on va printer p, on aura le même résultat que si l'on printant c,
			car ce que contient p, c'est l'adresse (lors du run, 0xffffc5dc)

Aussi,
p = (char *)&c;
pourrait s'écrire
p = c
car c est assimilable à un pointeur. A ce moment, le compilateur va traduire p = c comme cela :
p = &c[0]
Car notre array c[] est juste un truc démarrant à une case. Donc c == &c[0]
Et oui, oui, j'ai bien mis c == &c[0]

Donc p == &c et *p == c[0]

> p *c
(char) $3 = 'f'
> p *p
(char) $4 = 'f'

Pourquoi s'emmerder à pointer vers des adresses ?
Le programme, quand il tourne, il fonctionne dans un espace d'adressage réservé pour lui.
Dans cet espace d'adressage, il n'y a pas que le code exécuté, mais aussi par exemple les librairies utilisées.
Les librairies ne sont pas déclarées ici, mais il faut pouvoir y avoir accès.

Comment accède-t-on à quelque chose en mémoire ? En pointant dessus.

Si l'on ajoute la ligne suivante en header :
#include <stdio.h>
Et la ligne suivante avant le return i :
printf("%p\n", p);
De sorte que l'adresse de p soit affichée à chaque run de notre programme,
on obtient le résultat suivant si l'on fait dans lldb:
> p &printf
(int (*)(const char *__restrict, ...)) $0 = 0xf8a2e37e (libc.so.12 printf)
Ce qui nous donne l'adresse de printf.
Ainsi, même les fonctions sont des adresses.

Les autres utilités peuvent être... Tout. Y compris l'affichage à l'écran.
L'écran a en effet une adresse. L'utilisation des pointeurs permet donc
de faire référence à l'adresse d'un truc.
*/

	return i;
}
