# Vidéo de Passe-Science sur le hacking et les virus informatiques

## L'assembleur

En mémoire, les informations sont stockées en binaire, que l'on va représenter en blocs (octets) de 8 bits avec 1 bloc par ligne,
et plutôt que d'afficher une suite de 0 et de 1, on va préférer la représentation hexadécimale plus compacte, et chaque bloc aura une adresse en mémoire.
Une partie de ces adresses sera réservée à un programme, et une partie sera réservée aux données

```
binaire:	hexadécimal:	adresse:
...		...		...
00000001	01		01729f01 `| Programme : Chaque ligne correspond aux instructions, aux opérations logiques à réaliser.
11110000	f0		01729f02  | Par exemple, il peut interpréter c9 comme "Mets le contenu du registre B (f0) dans le registre A"
10101011	ab		01729f03  | Pour que ce soit lisible, on va le noter dans un langage compréhensible par l'humain, l'assembleur :
11001001	c9		01729f04  | mov eax, ebx #Ici, eax = c9
01001001	49		01729f05 ,|
...		...		...
10100000	a0		01730001 `|
11111111	ff		01730002 ,| Données
...
```

A côté de ceux-ci, il y a une collection de petits espaces mémoire techniques, appelés les registres,
différents d'un processeur à l'autre.

A	B	C
01	f0	ab

Les instructions courantes en assembleur sont :

* Les opérations de déplacement :
`mov eax, ebx`  -> Déplacement de la valeur du registre vers un autre registre
`mov eax, 01ff` -> Déplacement de la valeur du registre vers la mémoire
`mov 01ff, eax` -> Déplacement de la valeur en mémoire vers le registre

* Les opérations arithmétiques :
`add eax, ebx` -> Pour une addition
`mul eax, ebx` -> Pour une multiplication
`sub eax, ebx` -> Pour une substraction

Un programme devient intéressant lorsqu'on peut revenir à une instruction précédente, ou lorsqu'on peut faire appel à une instruction conditionnelle.
Pour cela, il y a des instructions spécifiques :

* Les opérations de saut :
```
	jmp 01ff--------|
	  ...		|
01ff: ..........<-------|
```

* Les opérations de saut conditionnel :
```
	...		= 0? ---|
	jz 01ff			|
	...	<----------non--♦
01ff: .........	<----------oui--|
```

Mais ce langage est assez complexe, et on lui préfèrera un langage plus haut niveau (C, C++), qui viendra à l'aide de son compilateur
convertir le code en assembleur (qui est et sera toujours le langage de la machine).

## Fonctions et piles

Un des concepts fondamentaux pour organiser un programme est celui de la fonction, une sous-séquence d'instructions que vous aimeriez pouvoir
rappeler à loisir à différents moments dans votre programme global. Mais cela pose un petit défi :
Une fois une fonction exécutée, on a envie de revenir là où elle était avant, mais comment le faire ? C'est là que rentre en jeu la notion de pile :

```
		Mémoire partie programme	|	Mémoire
		(assembleur)			|	"pile"
----------------------------------------------------------------------
		...
Appel de	01230000: call 01729f01
la fonction	01230001: <la suite>   ------------|
		...				   |
		01729f01: push ebp	<----------|
                01729f02: push ebp, esp		   |
                01729f03: push esp, fffffff0	   |	????????
Fonction	01729f04: sub esp, 60		   |	????????
                ...				   |	????????
		<beaucoup de code>		   |	????????
		...				   |	????????
                01730000: leave			   |-->	01230001
                01730001: ret				????????
```

Lorsqu'on appelle une fonction (eg. call), on pose sur la pile l'adresse à laquelle il faudra reprendre le programme en fin de fonction.
La fonction appelée va ajouter sur la pile de l'espace qu'elle utilisera pour ses variables (les multiples ???????? empilés).
Ainsi, une fonction utilise la pile comme un espace temporaire pour ses calculs.
Une fois la fonction terminée, le code se dépile et le code revient là où il faut (1230001)
Ce système permet à une fonction d'en appeler une autre, qui en appelle elle-même une autre.
Les contextes s'empilent et se dépilent progressivement au fur et à mesure de l'exécution des fonctions.

L'espace indiqué sur la pile peut correspondre à un user input.
Parfois, ce qui est utilisé par les hackers est d'inscrire des inputs ayant des espaces plus grands que l'espace alloué par la pile.
> Voir le papier "Smashing the Stack for Fun and Profit" (Aleph One, 1996).

# Retour au live
Rédaction du programme simple :
```
int
main()
{
	char chaine[] = "foo";
	char *pnt;

	pnt = chaine;
	// Le pointeur p va pointer vers l'adresse de ma variable chaine

	return 0;
}
```

On compile avec `cc -m32 -g -o ep5 ep5.c` puis on lance le débuggeur avec `lldb ep5` :
```
> b main
> r
> s
> p chaine
(char[4]) $0 = "foo"
> p &chaine
(char (*)[4]) $1 = 0xffffc5c0
> p &chaine[0]
(char *) $2 = 0xffffc5c0 "foo"
> s
> p pnt
(char *) $3 = 0xffffc5c0 "foo"
```

On ajoute ensuite la chaîne suivante sous `char chaine[]` :

	`char chaine2[] = {'b', 'a', 'r'};`

Ce qui équivaut à ce que fait `char chaine[]`, SAUF QU'ICI, ON N'A PAS DECLARE DE \0.
Dans `char chaine[]`, le \0 est déclaré de manière implicite. Pour qu'elles soient équivalentes, il faudrait `char chaine2[] = {'b', 'a', 'r', '\0'};`
NB : '' est utilisé pour les caractères uniques, "" pour les chaines.

Ainsi, si l'on déclarait :
`char chaine2[] = {"b", 'a', 'r'}`, on obtiendrait dans les faits :
`b\0ar`

Ensuite, une fois que l'on a déclaré le pointeur `pnt`, la ligne :
`pnt = chaine;`
Equivaut dans les faits à
`pnt = &chaine[0];`
On met l'adresse du premier élément de chaine dans `pnt`.

Lorsque dans le débuggeur, on regarde la valeur de notre pointeur, on obtient bien une adresse :
```
> p pnt
(char *) $0 = 0xffffc5e0 "foo"
```
La chose devient intéressante lorsqu'on s'intéresse à la valeur de la chaine :
```
> p chaine
(char[4]) $1 = "foo"
```
Toutefois, la réelle valeur du premier élément est la suivante :
```
> p &chaine[0]
(char *) $3 = 0xffffc5e0 "foo"
```
En réalité, chaine est donc un pointeur, mais déguisé.

De la même manière, si l'on compare la valeur de chaine[0] avec celle de pnt[0] :
```
(lldb) p chaine[0]
(char) $4 = 'f'
(lldb) p pnt[0]
(char) $5 = 'f'
```

Et donc, pour achever la comparaison :
```
(lldb) p &chaine[0]
(char *) $6 = 0xffffc5e0 "foo"
(lldb) p &pnt[0]
(char *) $7 = 0xffffc5e0 "foo"
```

La différence suivante est donc fallacieuse, une simple aide à la lecture du débuggeur.
```
(lldb) p chaine
(char[4]) $14 = "foo"
(lldb) p pnt
(char *) $15 = 0xffffc5e0 "foo"
```

Dans les faits, la valeur est rigoureusement la même.
```
> p &chaine
(char (*)[4]) $0 = 0xffffc5e0
> x/4b 0xffffc5e0
0xffffc5e0: 0x66 0x6f 0x6f 0x00
> p &chaine
(char (*)[4]) $0 = 0xffffc5e0
> x/4b 0xffffc5e0
0xffffc5e0: 0x66 0x6f 0x6f 0x00
> x/b $ebp-8
0xffffc5e0: 0x66
> x/4b $ebp-8
0xffffc5e0: 0x66 0x6f 0x6f 0x00
```

Par contre, regardons le retour de notre chaine2 (qui ne comportait pas de \0) :
```
> p &chaine2[0]
(char *) $4 = 0xffffc5dd "barfoo"
```

Si l'on regarde plus haut en mémoire :
```
> x/4b $ebp-12
0xffffc5dc: 0x00 0x62 0x61 0x72
//	     \0    b   a    r
> x/8b $ebp-12
0xffffc5dc: 0x00 0x62 0x61 0x72 0x66 0x6f 0x6f 0x00
//	     \0    b   a    r    f    o	   o    \0
```

Comme on n'a pas déclaré de \0 dans notre chaîne2, la suite de la stack après bar est donc logiquement foo.

```
(lldb) x 0xffffc5dd
0xffffc5dd: 62 61 72 66 6f 6f 00 e0 c5 ff ff 00 00 00 00 36  barfoo.........6
0xffffc5ed: df e2 f7 01 00 00 00 74 c6 ff ff 7c c6 ff ff 14  .......t...|....
(lldb) x 0xffffc5de
0xffffc5de: 61 72 66 6f 6f 00 e0 c5 ff ff 00 00 00 00 36 df  arfoo.........6.
0xffffc5ee: e2 f7 01 00 00 00 74 c6 ff ff 7c c6 ff ff 14 c6  ......t...|.....
(lldb) x 0xffffc5dc
0xffffc5dc: 00 62 61 72 66 6f 6f 00 e0 c5 ff ff 00 00 00 00  .barfoo.........
0xffffc5ec: 36 df e2 f7 01 00 00 00 74 c6 ff ff 7c c6 ff ff  6.......t...|...
```

# On passe au func5.c
On lldb le func5, puis on désassemble :
```
> disas
func5`stuff:
    0x804847d <+0>:  pushl  %ebp
    0x804847e <+1>:  movl   %esp, %ebp
->  0x8048480 <+3>:  movl   0x8(%ebp), %eax
    0x8048483 <+6>:  movb   $0x78, (%eax)
    0x8048486 <+9>:  nop
    0x8048487 <+10>: popl   %ebp
    0x8048488 <+11>: retl
```

On voit à la ligne <+3>, on manipule la stack, on peut même voir qu'on remonte la stack (revoir le schéma du live précédent dans le readme).

Si on revient sur la fonction main :
```
> disas -n main
func5`main:
    0x8048489 <+0>:  pushl  %ebp
    0x804848a <+1>:  movl   %esp, %ebp
    0x804848c <+3>:  subl   $0x10, %esp
    0x804848f <+6>:  movb   $0x62, -0x7(%ebp)
    0x8048493 <+10>: movb   $0x61, -0x6(%ebp)
    0x8048497 <+14>: movb   $0x72, -0x5(%ebp)
    0x804849b <+18>: leal   -0x7(%ebp), %eax		//J'empile eax dans esp avant l'appel de la fonction
    0x804849e <+21>: pushl  %eax
    0x804849f <+22>: calll  0x804847d                 ; stuff at func5.c:3:1
    0x80484a4 <+27>: addl   $0x4, %esp
    0x80484a7 <+30>: leal   -0x7(%ebp), %eax
    0x80484aa <+33>: movl   %eax, -0x4(%ebp)
    0x80484ad <+36>: movl   $0x0, %eax
    0x80484b2 <+41>: leave
    0x80484b3 <+42>: retl
```

Si on reprend donc l'image de la stack, on voit que les variables locales sont déclarées dans la pile après avoir chargé le programme précédent
(ici, main). Ainsi, stuff() intervient après main(), où ont déjà été chargées les variables qui sont ensuite déplacées, copiées...
Ainsi, si l'on va plus haut dans la stack et que l'on examine ce qu'il se trouve 8 octets après l'ebp (ebp qui a été déplacé sur stuff() et plus main()),
voici ce que l'on obtient:
```
> x/w $ebp+8
0xffffc5d4: 0xffffc5e1
> p (char *)0xffffc5e1
(char *) $1 = 0xffffc5e1 "bar@\x83\U00000004\b"
```
On retrouve bien notre bar ici, mais le bar à l'adresse pointée dans la fonction stuff.
Donc quand on passe un tableau comme argument de notre fonction stuff : `stuff(char c[])`, dans les faits, on passe un pointeur.

Le renvoi de la fonction stuff() sera ainsi un pointeur.
Donc dans les faits, plutôt que de déclarer
`stuff(char c[])`
On peut déclarer :
`stuff(char *c)`
Le code sera rigoureusement identique.

## On passe au pp.c
Admettons que maintenant, on souhaite déclarer un tableau de chaines de caractères. Par exemple, pas déclarer un tableau type {'a', 'b', 'c'},
mais davantage un tableau de type {"foo", "bar", "baz"}.

Pour cela, on déclare :
```
int
main()
{
	char *tc[] = {"foo", "bar"};

	return 0;
}
```

Cette ligne dit qu'on déclare un tableau dans lequel, au lieu d'avoir des char, on a des char* (des pointeurs vers des tableaux de chaînes de caractères).
On lance lldb :
```
> p tc
(char *[2]) $0 = ([0] = "foo", [1] = "bar")
> p tc[0]
(char *) $1 = 0x0804852c "foo"
> p tc[1]
(char *) $2 = 0x08048530 "bar"
```

Cependant, pour le compilateur, lorsqu'on passe un tableau en paramètre d'une autre fonction, en réalité on passe un pointeur sur la stack.
Ainsi, si l'on voulait passer ce tableau en argument, on ferait :
```
void
stuff(char **tc)
{
	return;
}

int
main()
{
	char *tc[] = {"foo", "bar"};

	stuff(tc);

	return 0;

}
```

Lorsqu'on regarde le programme :
```
> p tc
(char **) $0 = 0xffffc5e0
> p tc[0]
(char *) $1 = 0x0804853c "foo"
> x/w tc
0xffffc5e0: 0x0804853c
> x/w tc+1
0xffffc5e4: 0x08048540
> p (char *)0x08048540
(char *) $4 = 0x08048540 "bar"
```
