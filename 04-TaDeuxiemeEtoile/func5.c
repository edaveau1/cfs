void
stuff(char c[])
{
	c[0] = 'x';
}

int
main()
{
	char chaine2[] = {'b', 'a', 'r'};
	char *pnt;

	stuff(chaine2);

	pnt = chaine2;

	return 0;
}
