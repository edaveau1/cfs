# La stack
Dans un processus, il y a une zone de mémoire, la stack, prévue pour les variables locales aux fonctions.
Quand on déclare une variable, on l'empile.
Quand on empile, on décrémente les adresses.

A chaque fois qu'on rentre dans une fonction, on créé et on rentre dans une "frame" (stack frame) :
Une frame est une zone mémoire dans laquelle on va empiler les variables utilisées à un instant t par la fonction dans laquelle on est.
Cette frame existe pendant le temps d'existence de la fonction dans laquelle on est. La convention veut que quand on rentre dans une fonction,
on va sauvegarder le pointeur (l'adresse) de la précédente fonction pour pouvoir revenir dessus quand on a finit la fonction actuelle,
et on met nos variables dans cette pile.

L'originalité (un peu concon) de la pile, c'est qu'elle grossit vers le bas.

Le démarrage de la pile est en haut de la pile. Ce sont des adresses hautes. La dernière chose rentrée dans la pile aura une adresse plus haute
(eg. une variable i2 à 0xffffa100) que celle rentrée plus tôt dans le code (eg. une variable i1 à 0xffffa104).

# Le code en assembleur

L'assembleur est un langage assez simple avec des opérations basiques :
On peut bouger des valeurs d'un endroit à un autre, copier des valeurs, déplacer des valeurs...
En assembleur, il y a 2 zones mémoires à connaître :
        - Dans le CPU, il y a de petites cases mémoire, appelées des registres et qui vont très vite.
En 32 bits, les cases mémoire que l'on va utiliser sont appelées eax, ebx, edx... Qui sont des éléments dans la mémoire du CPU
que l'on va manipuler. Ces cases mémoire dans le CPU vont extrêmement vite.
        - La mémoire déclarée lorsque le processus démarre.
Dans cette zone mémoire, on va notamment retrouver la stack. Le CPU copie des trucs d'une zone mémoire (généralement la RAM) vers le CPU.

Pour résumer et garder en tête le diagramme de la stack du live :
[Stack bottom]
--- Earlier frames ↓ ---
[...]
--- Caller's frame ↓ ---
[...]
[Argument n]
[...]
[Argument 1]
[Return address]
--- Current frame ↓ ---
[Frame pointers %ebp]
[Saved registers, local variables and temporaries]
[Stack pointer %esp] <- Argument build area
[Stack "top"]

Compiler ce code, puis le lancer avec lldb :
> b main
> r
> disas -f
Ici, le -f indique que l'on veut désassembler la frame (la fonction dans laquelle je suis)

asm3`main:
    0x804847d <+0>:  pushl  %ebp <- Ici, on sauvegarde l'adresse du base pointer (présente dans le registre CPU) en RAM pour pouvoir y revenir à la fin de la fonction.
Pour la sauvegarder, on pousse cette adresse sur la pile (au tout début, et donc parmi les dernières adresses)
Push et pop sont 2 fonctions assembleur dont l'objectif pour push est de pousser le registre que l'on veut (eg %ebp) sur la stack).
Pop %ebp "pope" le dernier élément de la pile dans le registre ebp
    0x804847e <+1>:  movl   %esp, %ebp <- Le registre pointe vers l'adresse la plus basse dans la stack, %esp (stack pointer).
Ici, on change le point d'entrée de la fonction, %esp qui avait l'adresse la plus basse (première adresse dans la stack), devient donc %ebp (un peu plus loin dans la stack).
On met dans %ebp la valeur de %esp. %esp devient mon base pointer, le point d'entrée de ma nouvelle fonction.
mov est l'instruction en assembleur pour copier le pointeur de l'adresse la plus basse de la stack (%esp) dans %ebp
    0x8048480 <+3>:  subl   $0x10, %esp <- Je fais descendre mon stack pointer de 16 octets (Ox10). Je soustrais donc 16 octets à %esp pour déclarer des variables.
16 octets sont donc réservés pour i, c, *ptr. On peut voir sur le schéma précédent un espace réservé pour les "saved registers, local variables and temporaries"
Cela prouve que les variables n'existent pas. Ce sont des aides pour un humain, mais derrière, ce ne sont que des adresses.
Bien qu'on ne déclare que 3 variables (adresses) stockables sur 12 octets, 4 autres octets sont réservés pour le "canari", un système de protection de la stack,
et qui sont utiles à la compilation.
->  0x8048483 <+6>:  movl   $0x400, -0x4(%ebp)        ; imm = 0x400 <- Je copie 1024 4 octets (0x4) plus bas (les 4 octets sont réservés sous l'ebp)
Pour le vérifier dans lldb :
> x/4b $ebp-0x4
0xffffc624: 0x00 0x04 0x00 0x00
> p &i
(int *) $1 = 0xffffc5e4
Ce qui est égal dans notre code à
	int i = 1024;
Et en assembleur à
	0x8048483 <+6>:  movl   $0x400, -0x4(%ebp) 
    0x804848a <+13>: movl   $0x6f6f66, -0xc(%ebp)     ; imm = 0x6F6F66 <- Même chose, je copie mon tableau 12 octets (0xc) plus bas (! ne pas oublier de faire "s"
pour passer à l'étape suivante pour obtenir le bon résultat, sinon on aura d'autres octets !)
> x/4b $ebp-0xc
0xffffc5dc: 0x66 0x6f 0x6f 0x00
On a bien ici la représentation de "foo\o"
%ebp-0xc représente bien "foo", et pas l'adresse. "foo" est présent dans le code source et est copié directement en mémoire
16 octets plus bas que l'ebp dans la stack (0xc), dans le registre eax
https://keleshev.com/eax-x86-register-meaning-and-history/
X = Extended, EA = Type of register
Pour avoir l'adresse du f de foo :
> p/x $ebp-0xc
(unsigned int) $0 = 0xffffc5dc
> p &c[0]
(char *) $3 = 0xffffc5dc "foo"
    0x8048491 <+20>: leal   -0xc(%ebp), %eax <- lea signifie "charge l'adresse de l'opération %ebp-0xc" / Load Effective Address :
Instruction spécialement utilisée pour charger des adresses issues du calcul de l'adresse.
On rédige la chose suivante dans lldb :
> p $eax
(unsigned int) $0 = 4294952460
>p/x $eax
(unsigned int) $1 = 0xffffc60c
    0x8048494 <+23>: movl   %eax, -0x8(%ebp) <- On copie dans %ebp -0x8 qui est ptr l'adresse en question.
Dans ptr, il y a l'adresse du premier caractère de c.
A partir de là, ptr pointe sur la même valeur que c.
    0x8048497 <+26>: movzbl -0xb(%ebp), %eax
    0x804849b <+30>: movsbl %al, %eax <- %al équivaut à 0, on reset la valeur du registre de %eax en mémoire
    0x804849e <+33>: leave <- On rend les 16 octets, et on remet dans ebp le base pointer
    0x804849f <+34>: retl

Si notre chaîne avait fait plus de 4 caractères, il aurait fallu faire appel à un autre registre.
La convention en x86 est que la valeur de retour est placée dans le registre eax.
Dans notre code, on retourne c[1], qui est à %ebp-0xb
> x/3b $ebp-0xb
Le dernier caractère est bien 0x00, soit un code retour 0.
