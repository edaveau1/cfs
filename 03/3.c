int
main()
{
	int i = 1024;
	char c[4] = "foo";
	char *ptr;

	ptr = c;
	// ou ptr = c[0];
	// ou ptr = &c[0];
	// ou ptr = (char *)&c;

	return c[1];
}
