# Introduction
Le langage C est un langage compilé. C'est à dire que le code source est converti en binaire qui peut être comprit et exécuté par la machine. 
Le résultat d'un programme C sera un programme exécutable directement en langage machine (suite d'instructions en codes hexadécimaux interprétables par le CPU).
Le C décrit ce que l'on souhaite faire, et il revient au compilateur de traduire ce code C en code objet (qui sera in fine exécutable).

Pour voir le code d'un binaire lu par une machine :
`xxd /usr/mybin`

En C, lorsqu'on souhaite mettre des STOP à des endroits pour analyser le fonctionnement du code, on doit utiliser un débuggeur.

Ces 2 outils sont ceux vus aujourd'hui : Le compilateur et le débuggeur.
Sous UNIX, les deux principaux outils utilisés pour cela sont `gcc` et `gdb`, mais d'autres existent comme clang et lldb.

# Rédaction du 1.c
Un essai est réalisé en changeant i=0 par i=270. La valeur de retour de l'exécutable est ici 14.
On a un nombre limité de codes retour (127). Un code retour est codé sur 8 bits.

Une chose intéressante à faire est de lancer la commande
`./2 && echo plop`
En mettant comme valeur i = 0 et i = 1. Dans le cas où i = 0, "plop" sera bien affiché, car le code retour est 0. Mais dans le cas où i = 1, "plop" ne s'affichera pas.

Une fois le fichier compilé, on peut lancer notre premier breakpoint sur la fonction main :
T0286633-A@ARTREN649906L ~/cfromscratch/01-LesOutilsDeLaCréation $ gdb 1
(gdb) b main
Breakpoint 1 at 0x40053a

On va le compiler en 32 bits (pour pouvoir jouer avec les adresses plus tard)
Pour compiler en 32 bits : 
`sudo yum install glibc-devel.i686 libgcc.i686 libstdc++-devel.i686 ncurses-devel.i686`

Le -g permet d'avoir des symboles de débuggage.
`cc -m32 -g -o 1 1.c`

Puis lancer gdb:
gdb 1
> layout src #Pour afficher le code
> b main #Pour faire un breakpoint sur main
> r #Pour run

# Petit point sur les variables
Une fois le code rédigé et compilé et que le programme est devenu du code machine : Les variables n'existent pas.
Les variables sont du "sucre syntaxique". Ce ne sont que des facilités pour qu'un humain puisse programmer.
Dans les faits, une variable est une façon human-readable d'exprimer une adresse.
Dans un débugger, autant qu'en C, pour accéder à l'adresse, on met le caractère &.

&i donnera l'adresse de i.

Dans le débugger, après le run (r) :
> s
> p &i #Print l'adresse de i
> p i #Print la valeur de i
> p main #Print l'adresse de main
> p &main #Renvoie le même output que p main
> p sizeof(i) #Renvoie la taille de l'int (ici, 4 octets, soit 32 bits car on a compilé en 32 bits)

# Rédaction du 2.c
Lorsqu'on compile le 2.c et qu'on l'exécute, la valeur retour de `echo $?` est 97 (valeur de a en ASCII (voir sur les tables ASCII)).
Relancer ensuite gdb 2 :
> layout src
> b main
> r
> s #pour step
> p c
> p c[0] #Affiche la valeur en mémoire
> p c[1]
> p c[2]
> p c[3]
> p c[4] #On retrouve bien le \0
> p &c
> p &c[1] #Equivaut à l'adresse de p &c[0] + 1 et renvoie "bcd\0" => C'est un pointeur vers l'emplacement mémoire de c
	  #Ce qui explique pourquoi le reste de la chaîne est renvoyé
> p *<adresse renvoyée ci-dessus>
