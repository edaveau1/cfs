# L'histoire de C
 
Dans les années 1970, deux personnes vont travailler sur l'OS UNIX, Dennis Ritchie et Ken Thompson travaillent sur un mini-ordinateur, le PDP-7. UNIX est fait en assembleur, le langage le plus proche de la machine, composé de mots simples, par exemple :

- __MOV__: Move, ce qui revient à copier des données d'un endroit à un autre.
- __IN__: Input from port
- __OUT__: Output to port
- __AND__: AND Logique
- __OR__: OR Logique
- __WAIT__: Wait until not busy
- ...

Ces expressions étant très simples, la manipulation de ces éléments pour réaliser des opérations de haut niveau peut être très long. De plus, les langages assembleurs changent en fonction des processeurs.  A l'époque, UNIX n'existe que sur PDP-7. Ritchie et Thompson reçoivent un jour un PDP-11. Cependant, le portage d'UNIX du PDP-7 vers PDP-11 apparaît comme une difficulté majeure. 
Ritchie et Thompson souhaitent alors un langage de haut niveau permettant d'éviter d'avoir à porter l'assembleur vers de nouvelles machines. Il existait à cette époque un langage, le langage B, qui était l'ancêtre du C, lui-même dérivé d'un langage appelé BCPL et développé pour MULTICS.

Cependant, le B n'était pas trop abouti. Ce langage ne contenait pas de compilateur, juste un interpréteur. Cela signifie qu'il ne générait pas de code pour la machine sur laquelle il tournait. Ce n'était qu'un programme interprétant ce que le code essayait de dire, rendant le lancement de code assez lent. Ritchie et Thompson décident donc d'écrire un langage permettant d'atteindre leurs objectifs. Au bout de 6 mois, ils réussirent à réécrire UNIX en C.

C'était ainsi un langage possédant un compilateur permettant de générer du code adapté à la machine sur laquelle il fonctionnait, cela même pour un système d'exploitation.