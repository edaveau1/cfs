Jusqu'ici, nous sommes resté sur une fonction main simple, sans arguments. Mais elle peut en prendre, et en règle générale, on lui en passe.
Habituellement, on lui passe :

```
int
main(int argc, char *argv[])
{
	return 0;
}
```

* int argc : Correspond au nombre d'arguments que l'on va passer à la fonction
* char *argv[] : Qui est lui un tableau de pointeurs

Exemple fictif de ce qu'il se passe en mémoire :

```
Adresse fictive :	1000	1004	1008	...		// Les pointeurs ont une taille de 4 octets.
Stack : 	      [ [argv0]	[argv1]	[argv2] [...] ]
```

argv0 est un pointeur, et il contient bien une adresse (eg. 1200), qui pointe vers un argument (eg /bin/ls).
argv1 quand à lui pourrait pointer vers 1207, qui contient l'argument "-l".

A savoir, on pourrait parfaitement remplacer `char *argv[]` par `char **argv`, mais pour rappeler que c'est bien un tableau, on préfèrera la première notation.
La notation `char *argv[]` peut ainsi se lire "tableau de pointeurs sur des char".

On lance ensuite la compilation avec `cc -m32 -O0 -g -o args -fno-pie args.c`, puis on lance le débuggeur :
> gef config context.grow_stack_down True
> b main
> r 1 2
0xffffc4d4│+0x001c: 0x00000000
0xffffc4d0│+0x0018: 0x0804a00c  →  0xf7e2de40  →  <__libc_start_main+0> endbr32 
0xffffc4cc│+0x0014: 0xffffc4e4  →  0xf7fb4000  →  0x001a3d6c
0xffffc4c8│+0x0010: 0xffffc554  →  0xffffc79b  →  "LS_COLORS=rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;3[...]"
0xffffc4c4│+0x000c: 0xffffc544  →  0xffffc75f  →  "/home/T0286633-A/cfromscratch/06-DownTheRabbitHole[...]"
0xffffc4c0│+0x0008: 0x00000003
0xffffc4bc│+0x0004: 0xf7e2df36  →  <__libc_start_main+246> add esp, 0x10
0xffffc4b8│+0x0000: 0x00000000	 ← $esp, $ebp

On peut voir notre base pointer (à 0x0004), et à 0x0008, on retrouve nos arguments.
NB: L'output de gef est différent de celui du live, car on n'a pas notre adresse retour entre l'argument et le base pointer.

Le 0x00000003 correspond à argc. Ensuite, à l'adresse 0xffffc4c4, on a argv :
```
> context
[#0] 0x8048480 → main(argc=0x3, argv=0xffffc544)
```

Si on reprend les informations dans la stack :
`0xffffc4c4│+0x000c: 0xffffc544`
Le pointeur pour argv, présent dans la stack à 0x000c et dans la mémoire à 0xffffc4c4 pointe bien vers l'adresse 0xffffc544 (argv), qui contient le tableau de valeurs. 
Dans ce tableau, la première valeur est 0xffffc75f.

Si on examine cette valeur :
```
> x/w 0xffffc544
0xffffc544:	0xffffc75f
```

Si on examine ensuite la valeur de cette adresse :
```
> p (char *)0xffffc75f
$5 = 0xffffc75f "/home/T0286633-A/cfromscratch/06-DownTheRabbitHole/args"
```

On retrouve notre premier argument.

Le tableau est un tableau de pointeurs, donc un tableau qui a la taille des registres que l'on va utiliser (ici, 32 bits, donc 4 octets).
Ainsi, si l'on regarde ce qu'il y a 4 octets plus loin dans le tableau (0xffffc544 + 4 octets) :

```
gef➤  x/w 0xffffc548
0xffffc548:	0xffffc797
gef➤  p (char *)0xffffc797
$7 = 0xffffc797 "1"
```

On retrouve bien la valeur de notre premier argument.

On modifie ensuite les arguments passés pour mettre à la place des chaînes plus explicites :
```
> r AAAA BBBB
0xffffc4d4│+0x001c: 0x00000000
0xffffc4d0│+0x0018: 0x0804a00c  →  0xf7e2de40  →  <__libc_start_main+0> endbr32 
0xffffc4cc│+0x0014: 0xffffc4e4  →  0xf7fb4000  →  0x001a3d6c
0xffffc4c8│+0x0010: 0xffffc554  →  0xffffc79b  →  "LS_COLORS=rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;3[...]"
0xffffc4c4│+0x000c: 0xffffc544  →  0xffffc759  →  "/home/T0286633-A/cfromscratch/06-DownTheRabbitHole[...]" <=== pointeur vers argv
0xffffc4c0│+0x0008: 0x00000003		<=== argc
0xffffc4bc│+0x0004: 0xf7e2df36  →  <__libc_start_main+246> add esp, 0x10
0xffffc4b8│+0x0000: 0x00000000	 ← $esp, $ebp
```

On examine le contenu de l'adresse vers laquelle pointe notre argv (0xffffc544) :
```
> x/w 0xffffc544
0xffffc544:	0xffffc759
> p (char *)0xffffc759
$8 = 0xffffc759 "/home/T0286633-A/cfromscratch/06-DownTheRabbitHole/args" /*== argv[0]*/
> x/w 0xffffc548 /*4 octets plus loin que l'adresse du premier pointeur*/
0xffffc548:	0xffffc791
> p (char *)0xffffc791
$11 = 0xffffc791 "AAAA"	/*== argv[1]*/
> x/w 0xffffc54c
0xffffc54c:	0xffffc796
> p (char *)0xffffc796
$12 = 0xffffc796 "BBBB"
```

Comment connaît-on la fin d'un tableau ?
Si on regarde encore 4 octets plus loin :
```
x/w 0xffffc550
0xffffc550:	0x00000000
```
On a bien du NULL, indiquant la fin (d'une chaîne de caractères, d'un tableau...).

Et si on regarde encore 4 octets plus loin :
```
> x/w 0xffffc554
0xffffc554:	0xffffc79b
> p (char *)0xffffc79b
$13 = 0xffffc79b "LS_COLORS=rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;38;5;11:so=38;5;13:do=38;5;5:bd=48;5;232;38;5;11:cd=48;5;232;38;5;3:or=48;5;232;38;5;9:mi=01;05;37;41:su=48;5;196;38;5;15:sg=48;5;11;38;5;16:ca=48;5;196;38;5;226:tw=48;5;10;38;5;16:ow=48;5;10;38;5;21:st=48;5;21;38;5;15:ex=38;5;40:*.tar=38;5;9:*.tgz=38;5;9:*.arc=38;5;9:*.arj=38;5;9:*.taz=38;5;9:*.lha=38;5;9:*.lz4=38;5;9:*.lzh=38;5;9:*.lzma=38;5;9:*.tlz=38;5;9:*.txz=38;5;9:*.tzo=38;5;9:*.t7z=38;5;9:*.zip=38;5;9:*.z=38;5;9:*.dz=38;5;9:*.gz=38;5;9:*.lrz=38;5;9:*.lz=38;5;9:*.lzo=38;5;9:*.xz=38;5;9:*.zst=38;5;9:*.tzst=38;5;9:*.bz2=38;5;9:*.bz=38;5;9:*.tbz=38;5;9:*.tbz2=38;5;9:*.tz=38;5;9:*.deb=38;5;9:*.rpm=38;5;9:*.jar=38;5;9:*.war=38;5;9:*.ear=38;5;9:*.sar=38;5;9:*.rar=38;5;9:*.alz=38;5;9:*.ace=38;5;9:*.zoo=38;5;9:*.cpio=38;5;9:*.7z=38;5;9:*.rz=38;5;9:*.cab=38;5;9:*.wim=38;5;9:*.swm=38;5;9:*.dwm=38;5;9:*.esd=38;5;9:*.jpg=38;5;13:*.jpeg=38;5;13:*.mjpg=38;5;13:*.mjpeg=38;5;13:*.gif=38;5;13:*.bmp=38;5;13:*.pbm=38;5;13:*.pgm=38;5;13:*.ppm=38;5;13:*.tga=38;5;13:*.xbm=38;5;13:*.xpm=38;5;13:*.tif=38;5;13:*.tiff=38;5;13:*.png=38;5;13:*.svg=38;5;13:*.svgz=38;5;13:*.mng=38;5;13:*.pcx=38;5;13:*.mov=38;5;13:*.mpg=38;5;13:*.mpeg=38;5;13:*.m2v=38;5;13:*.mkv=38;5;13:*.webm=38;5;13:*.ogm=38;5;13:*.mp4=38;5;13:*.m4v=38;5;13:*.mp4v=38;5;13:*.vob=38;5;13:*.qt=38;5;13:*.nuv=38;5;13:*.wmv=38;5;13:*.asf=38;5;13:*.rm=38;5;13:*.rmvb=38;5;13:*.flc=38;5;13:*.avi=38;5;13:*.fli=38;5;13:*.flv=38;5;13:*.gl=38;5;13:*.dl=38;5;13:*.xcf=38;5;13:*.xwd=38;5;13:*.yuv=38;5;13:*.cgm=38;5;13:*.emf=38;5;13:*.ogv=38;5;13:*.ogx=38;5;13:*.aac=38;5;45:*.au=38;5;45:*.flac=38;5;45:*.m4a=38;5;45:*.mid=38;5;45:*.midi=38;5;45:*.mka=38;5;45:*.mp3=38;5;45:*.mpc=38;5;45:*.ogg=38;5;45:*.ra=38;5;45:*.wav=38;5;45:*.oga=38;5;45:*.opus=38;5;45:*.spx=38;5;45:*.xspf=38;5;45:"
```

Ce qui correspond aux variables d'environnement. Ce comportement est du au fait qu'il y a un troisième argument qui n'est jamais explicitement nommé :
`main(int argc, char *argv[], char *envp)`

On modifie le code, on recompile, on redébugge :
```
[#0] 0x8048480 → main(argc=0x3, argv=0xffffc544, envp=0xffffc554)

gef➤  x/w 0xffffc544
0xffffc544:	0xffffc759
gef➤  p (char *)0xffffc759
$1 = 0xffffc759 "/home/T0286633-A/cfromscratch/06-DownTheRabbitHole/args"
gef➤  x/w 0xffffc548
0xffffc548:	0xffffc791
gef➤  p (char *)0xffffc791
$2 = 0xffffc791 "AAAA"
gef➤  x/w 0xffffc54c
0xffffc54c:	0xffffc796
gef➤  p (char *)0xffffc796
$3 = 0xffffc796 "BBBB"
gef➤  x/w 0xffffc550
0xffffc550:	0x0
gef➤  x/w 0xffffc554
0xffffc554:	0xffffc79b
gef➤  p (char *)0xffffc79b
$4 = 0xffffc79b "LS_COLORS=rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;38;5;11:so=38;5;13:do=38;5;5:bd=48;5;232;38;5;11:cd=48;5;232;38;5;3:or=48;5;232;38;5;9:mi=01;05;37;41:su=48;5;196;38;5;15:sg=48;5;11;38;5;16:ca=48;5;196;38;5;226:tw=48;5;10;38;5;16:ow=48;5;10;38;5;21:st=48;5;21;38;5;15:ex=38;5;40:*.tar=38;5;9:*.tgz=38;5;9:*.arc=38;5;9:*.arj=38;5;9:*.taz=38;5;9:*.lha=38;5;9:*.lz4=38;5;9:*.lzh=38;5;9:*.lzma=38;5;9:*.tlz=38;5;9:*.txz=38;5;9:*.tzo=38;5;9:*.t7z=38;5;9:*.zip=38;5;9:*.z=38;5;9:*.dz=38;5;9:*.gz=38;5;9:*.lrz=38;5;9:*.lz=38;5;9:*.lzo=38;5;9:*.xz=38;5;9:*.zst=38;5;9:*.tzst=38;5;9:*.bz2=38;5;9:*.bz=38;5;9:*.tbz=38;5;9:*.tbz2=38;5;9:*.tz=38;5;9:*.deb=38;5;9:*.rpm=38;5;9:*.jar=38;5;9:*.war=38;5;9:*.ear=38;5;9:*.sar=38;5;9:*.rar=38;5;9:*.alz=38;5;9:*.ace=38;5;9:*.zoo=38;5;9:*.cpio=38;5;9:*.7z=38;5;9:*.rz=38;5;9:*.cab=38;5;9:*.wim=38;5;9:*.swm=38;5;9:*.dwm=38;5;9:*.esd=38;5;9:*.jpg=38;5;13:*.jpeg=38;5;13:*.mjpg=38;5;13:*.mjpeg=38;5;13:*.gif=38;5;13:*.bmp=38;5;13:*.pbm=38;5;13:*.pgm=38;5;13:*.ppm=38;5;13:*.tga=38;5;13:*.xbm=38;5;13:*.xpm=38;5;13:*.tif=38;5;13:*.tiff=38;5;13:*.png=38;5;13:*.svg=38;5;13:*.svgz=38;5;13:*.mng=38;5;13:*.pcx=38;5;13:*.mov=38;5;13:*.mpg=38;5;13:*.mpeg=38;5;13:*.m2v=38;5;13:*.mkv=38;5;13:*.webm=38;5;13:*.ogm=38;5;13:*.mp4=38;5;13:*.m4v=38;5;13:*.mp4v=38;5;13:*.vob=38;5;13:*.qt=38;5;13:*.nuv=38;5;13:*.wmv=38;5;13:*.asf=38;5;13:*.rm=38;5;13:*.rmvb=38;5;13:*.flc=38;5;13:*.avi=38;5;13:*.fli=38;5;13:*.flv=38;5;13:*.gl=38;5;13:*.dl=38;5;13:*.xcf=38;5;13:*.xwd=38;5;13:*.yuv=38;5;13:*.cgm=38;5;13:*.emf=38;5;13:*.ogv=38;5;13:*.ogx=38;5;13:*.aac=38;5;45:*.au=38;5;45:*.flac=38;5;45:*.m4a=38;5;45:*.mid=38;5;45:*.midi=38;5;45:*.mka=38;5;45:*.mp3=38;5;45:*.mpc=38;5;45:*.ogg=38;5;45:*.ra=38;5;45:*.wav=38;5;45:*.oga=38;5;45:*.opus=38;5;45:*.spx=38;5;45:*.xspf=38;5;45:"
gef➤  p envp
$5 = (char **) 0xffffc554
```

La première adresse inclue dans le tableau de pointeur est l'adresse qui suit argv (ici, 0xffffc554). 
Le "tableau" (la suite d'adresses en mémoire) envp est donc celui qui suit le "tableau" (la suite d'adresses en mémoire) argv.
Ainsi, 4 octets plus loin que la fin d'argv, on tombe sur le tableau de pointeur vers les variables d'environnement.
Plus on avancera dans le tableau, plus on va afficher de variables d'environnement, jusqu'à tomber sur un 0x00.

Ainsi :

```
gef➤  x/w 0xffffc544,
A syntax error in expression, near `'.
gef➤  x/w 0xffffc544
0xffffc544:	0xffffc759
gef➤  p (char *)0xffffc759
$6 = 0xffffc759 "/home/T0286633-A/cfromscratch/06-DownTheRabbitHole/args"
gef➤  x/w 0xffffc548
0xffffc548:	0xffffc791	<= argv[0] = AAAA
gef➤  x/w 0xffffc54c
0xffffc54c:	0xffffc796	<= argv[1] = BBBB
gef➤  x/w 0xffffc550
0xffffc550:	0x0		<= Fin de argv
gef➤  x/w 0xffffc554
0xffffc554:	0xffffc79b	<= Début de envp
gef➤  p (char *)0xffffc79b
$7 = 0xffffc79b "LS_COLORS=rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;38;5;11:so=38;5;13:do=38;5;5:bd=48;5;232;38;5;11:cd=48;5;232;38;5;3:or=48;5;232;38;5;9:mi=01;05;37;41:su=48;5;196;38;5;15:sg=48;5;11;38;5;16:ca=48;5;196;38;5;226:tw=48;5;10;38;5;16:ow=48;5;10;38;5;21:st=48;5;21;38;5;15:ex=38;5;40:*.tar=38;5;9:*.tgz=38;5;9:*.arc=38;5;9:*.arj=38;5;9:*.taz=38;5;9:*.lha=38;5;9:*.lz4=38;5;9:*.lzh=38;5;9:*.lzma=38;5;9:*.tlz=38;5;9:*.txz=38;5;9:*.tzo=38;5;9:*.t7z=38;5;9:*.zip=38;5;9:*.z=38;5;9:*.dz=38;5;9:*.gz=38;5;9:*.lrz=38;5;9:*.lz=38;5;9:*.lzo=38;5;9:*.xz=38;5;9:*.zst=38;5;9:*.tzst=38;5;9:*.bz2=38;5;9:*.bz=38;5;9:*.tbz=38;5;9:*.tbz2=38;5;9:*.tz=38;5;9:*.deb=38;5;9:*.rpm=38;5;9:*.jar=38;5;9:*.war=38;5;9:*.ear=38;5;9:*.sar=38;5;9:*.rar=38;5;9:*.alz=38;5;9:*.ace=38;5;9:*.zoo=38;5;9:*.cpio=38;5;9:*.7z=38;5;9:*.rz=38;5;9:*.cab=38;5;9:*.wim=38;5;9:*.swm=38;5;9:*.dwm=38;5;9:*.esd=38;5;9:*.jpg=38;5;13:*.jpeg=38;5;13:*.mjpg=38;5;13:*.mjpeg=38;5;13:*.gif=38;5;13:*.bmp=38;5;13:*.pbm=38;5;13:*.pgm=38;5;13:*.ppm=38;5;13:*.tga=38;5;13:*.xbm=38;5;13:*.xpm=38;5;13:*.tif=38;5;13:*.tiff=38;5;13:*.png=38;5;13:*.svg=38;5;13:*.svgz=38;5;13:*.mng=38;5;13:*.pcx=38;5;13:*.mov=38;5;13:*.mpg=38;5;13:*.mpeg=38;5;13:*.m2v=38;5;13:*.mkv=38;5;13:*.webm=38;5;13:*.ogm=38;5;13:*.mp4=38;5;13:*.m4v=38;5;13:*.mp4v=38;5;13:*.vob=38;5;13:*.qt=38;5;13:*.nuv=38;5;13:*.wmv=38;5;13:*.asf=38;5;13:*.rm=38;5;13:*.rmvb=38;5;13:*.flc=38;5;13:*.avi=38;5;13:*.fli=38;5;13:*.flv=38;5;13:*.gl=38;5;13:*.dl=38;5;13:*.xcf=38;5;13:*.xwd=38;5;13:*.yuv=38;5;13:*.cgm=38;5;13:*.emf=38;5;13:*.ogv=38;5;13:*.ogx=38;5;13:*.aac=38;5;45:*.au=38;5;45:*.flac=38;5;45:*.m4a=38;5;45:*.mid=38;5;45:*.midi=38;5;45:*.mka=38;5;45:*.mp3=38;5;45:*.mpc=38;5;45:*.ogg=38;5;45:*.ra=38;5;45:*.wav=38;5;45:*.oga=38;5;45:*.opus=38;5;45:*.spx=38;5;45:*.xspf=38;5;45:"
gef➤  p (char *)0xffffce99
$8 = 0xffffce99 "GIT_USER=username"
...
Affichage de toutes mes variables d'environnement
...
gef➤  x/w 0xffffc674
0xffffc674:	0xffffdf9a
gef➤  p (char *)0xffffdf9a
$40 = 0xffffdf9a "SOME_VAR=SOME_VALUE"
gef➤  x/w 0xffffc678
0xffffc678:	0x0
```
