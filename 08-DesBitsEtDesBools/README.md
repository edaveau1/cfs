# Atx.c
On revient sur la notion d'exit et la différence avec exit en commençant par un petit code simple.
Aussi, parce que ça n'a pas encore été écrit, la fonction main est appelée par la fonction start() de la libc.
In fine, exit et return aboutissent peu au prou à la même situation (eg. `exit(0);` renvoie le code 0, `exit(1);` renvoie le code 1...).
En fin de code, on pourrait tout aussi bien écrire `return 0;` que `exit(0);`. Déjà, premier souci, `exit(0);` est une fonction de la libc, alors que return est du C.

Exit() est dans stdlib.h.

Par contre, il y a une grosse différence. Lorsqu'on sort du programme avec return, on return à une fonction appelante. 
Main est une fonction appelée par une fonction précédente. Quand on fait return 0, on revient à cette fonction.
Quand on revient à cette fonction, il y a du ménage qui est fait.

Quand on appelle exit(), on fait l'appel système exit() qui bute le programme sans revenir à la fonction appelante.
Lorsqu'on appelle exit, juste avant il se passe un truc : En caché, `exit()` appelle la fonction `atexit()`.

atx.c
```
# include <stdlib.h>
# include <unistd.h>

char *msg;

void
xxx()
{
	write(1, msg, 4);
}

int
main()
{
	char cmsg[] = "foo\n";
	msg = cmsg;

	atexit(xxx);
	exit(0);
}
```

unistd est la librairie contenant le prototype officiel de write.
Ici, quand on va appeler la fonction exit(), qui appelle la fonction atexit(), on va faire appel à la fonction xxx avant.
Le lancement de `./atx` renvoie bien `foo`.

Maintenant, si à la place d'exit(0), on return et on lance atx, l'output n'affiche pas "foo".
Le return retourne la fonction appelante qui elle fait le ménage sur la stack.
Que exit() ou return soit utilisé, la fonction atexit est appelée.

Pour le coup, faut-il utiliser return ou exit ?
Si l'on s'en réfère aux guides de développement de NetBSD :
*"Since main is a function that returns an int, prefer returning from it, than calling exit."*

Mais par exemple, FreeBSD et OpenBSD utilisent exit. Ainsi, ce n'est pas mal en soi d'utiliser exit.

# Ret.c
On passe désormais à la rédaction de ret.c
```
int
main()
{ 
        return 1;
}
```

On essaie avec 1, 200 et 255 et 256.
Le hic, c'est que 256 renvoie 0.
257 renvoie 1.

Parce que la valeur max de ff est 255 (16^2).

Si on va regarder dans les sources du kernel la fonction exit, le code du syscall exit fait la chose suivante :
```
SYSCALL_DEFINE1(exit, int, error_code)
{
	do_exit((error_code&0xff)<<8);
}
```

C'est une histoire de bits et de bools.

En informatique, on ne fait que gérer des booléens, des 0 et des 1, avec des opérateurs :
1 & 1 = 1
1 & 0 = 0
0 & 1 = 0
0 & 0 = 0

_Quel rapport avec &ff ?_

Admettons que pour une raison qu'on verra plus tard, on a besoin d'avoir un maximum de 255 sur un type de valeur.
Si j'ai des bits (...10111111111), mais que je ne suis intéressé que de récupérer les 8 premiers bits (11111111),
il faut donc annuler les autres bits. Une façon de faire ça est de dire :
```
  8 bits  |  8 bits
 _________|_________
 .....101 | 11111111
&00000000  &11111111
```

Quand j'ajoute ce masque (à droite, le masque est à 0xff), ce masque sélectionne tous les bits et annule ceux de gauche, car implicitement ils sont tous à 00000000.

Un autre exemple de masque :

```
 01001101 | 01001110
&00000000  &11111111
--------------------
 00000000   01001110
```

Dans ce deuxième exemple, on récupère bien notre valeur de retour initiale en virant les 8 premiers bits.
255 est donc égal à 11111111
256 est lui égal à 100000000
Avec le masque :   -00000000
Le code retour est donc de 0.

On change maintenant le code de ret.c ainsi : `return 1 & 1;`
1 & 1 va forcément nous donner 1, car le masque 1 avec le 1 va nous amener à 1 (cf. le tableau plus haut).

Si maintenant on change pour `return 0 & 1;`, le code retour est donc 0.

Maintenant, si on prend les valeurs suivantes :
```
00010001 #Soit 17, masqué avec :
00001111 #Soit 15
--------
00000001
```

On essaie avec `return 17 & 15;`

Dans le code kernel, le masque est donc à 0xff, et va donc effacer tout ce qui est supérieur à 255 dans le code de retour.
`(error_code&0xff)` => Le code retour est masqué au delà de 255.

Intéressons nous maintenant à la deuxième partie de ce code :
`<<8`

Le << est appelé un décalage :
```
0001	= 1 en base 10, 1(10)
	<<1 viendra décaler 1 bit, ce qui nous donne :
0010	= 2(10)
	<<1
0100	= 4(10)
```

Le décalage évite les multiplications, le CPU est très performant pour les décalages, et ça fait de la place. 
Le décalage décale toujours vers des 0.

Mais ça permet autre chose :
```
0000000000000001
		  <<8
0000000100000000
```

On a donc de la place sur nos 8 premiers bits (00000000) pour mettre une autre valeur (eg. la raison pour laquelle un processus a quitté).

La raison du `((error_code&0xff)<<8)`, c'est

* Pour accéder au code erreur de notre processus
* Pour être à la disposition de la fonction wait

On verra dans le prochain épisode à quoi tout cela sert.
